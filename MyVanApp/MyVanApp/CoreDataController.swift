//
//  CoreDataController.swift
//  MyVanApp
//
//  Created by Moses Olawoyin on 11/12/2019.
//  Copyright © 2019 Moses Olawoyin. All rights reserved.
//

import Foundation
import CoreData

class CoreDataController {
    private init() {}
    
    static let shared = CoreDataController()
    
    var mainContex: NSManagedObjectContext {
        return persistentContainer.viewContext
    }
    
    //CoreData Stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "ItemCoreData")
        container.loadPersistentStores(completionHandler: {(StoreDescription, error) in
            if let error = error as NSError?{
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
            
        })
        return container
    }()
    
    //CoreData saving support
      func saveContext() -> Bool {
          let context = persistentContainer.viewContext
          if context.hasChanges {
              do{
                  try context.save()
                  return true
              }catch{
                  let nserror = error as NSError
                  
                  fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
              }
          }
          return false
      }
    
}
